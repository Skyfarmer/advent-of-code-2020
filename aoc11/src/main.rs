extern crate bytecount;

use std::fs::File;
use std::io::{BufReader, Read};

const ROW_SIZE: usize = 90;

enum Direction {
    Up,
    Down,
    Left,
    Right,
    LeftUp,
    LeftDown,
    RightUp,
    RightDown,
}

fn check_value(input: u8) -> Option<bool> {
    match input {
        b'#' => Some(true),
        b'L' => Some(false),
        _ => None,
    }
}

fn occupied_vision(input: &[u8], i: usize, d: Direction, limit: Option<usize>) -> bool {
    for counter in 0..limit.unwrap_or(usize::MAX) {
        match d {
            Direction::Up => {
                if i.wrapping_sub(ROW_SIZE * (counter + 1)) > i {
                    break;
                } else if let Some(v) = check_value(input[i - ROW_SIZE * (counter + 1)]) {
                    return v;
                }
            }
            Direction::Down => {
                if i + ROW_SIZE * (counter + 1) >= input.len() {
                    break;
                } else if let Some(v) = check_value(input[i + ROW_SIZE * (counter + 1)]) {
                    return v;
                }
            }
            Direction::Left => {
                if i.wrapping_sub(counter + 1) % ROW_SIZE > i % ROW_SIZE {
                    break;
                } else if let Some(v) = check_value(input[i - counter - 1]) {
                    return v;
                }
            }
            Direction::Right => {
                if (i + counter + 1) % ROW_SIZE < i % ROW_SIZE {
                    break;
                } else if let Some(v) = check_value(input[i + counter + 1]) {
                    return v;
                }
            }
            Direction::LeftUp => {
                if i.wrapping_sub(counter + 1) % ROW_SIZE > i % ROW_SIZE
                    || i.wrapping_sub(ROW_SIZE * (counter + 1)) > i
                {
                    break;
                } else if let Some(v) =
                    check_value(input[i - ROW_SIZE * (counter + 1) - counter - 1])
                {
                    return v;
                }
            }
            Direction::LeftDown => {
                if i.wrapping_sub(counter + 1) % ROW_SIZE > i % ROW_SIZE
                    || i + ROW_SIZE * (counter + 1) >= input.len()
                {
                    break;
                } else if let Some(v) =
                    check_value(input[i + ROW_SIZE * (counter + 1) - counter - 1])
                {
                    return v;
                }
            }
            Direction::RightUp => {
                if (i + 1) % ROW_SIZE < i % ROW_SIZE || i.wrapping_sub(ROW_SIZE * (counter + 1)) > i
                {
                    break;
                } else if let Some(v) =
                    check_value(input[i - ROW_SIZE * (counter + 1) + counter + 1])
                {
                    return v;
                }
            }
            Direction::RightDown => {
                if (i + 1) % ROW_SIZE < i % ROW_SIZE || i + ROW_SIZE * (counter + 1) >= input.len() {
                    break;
                } else if let Some(v) =
                    check_value(input[i + ROW_SIZE * (counter + 1) + counter + 1])
                {
                    return v;
                }
            }
        }
    }

    false
}

fn occupied(input: &[u8], i: usize, limit: Option<usize>) -> u32 {
    let mut occupied = 0;

    //Left row is available
    if occupied_vision(input, i, Direction::Left, limit) {
        //Left item
        occupied += 1;
    }
    if occupied_vision(input, i, Direction::LeftUp, limit) {
        //Top Left item
        occupied += 1;
    }
    if occupied_vision(input, i, Direction::LeftDown, limit) {
        //Bottom left item
        occupied += 1;
    }
    if occupied_vision(input, i, Direction::Right, limit) {
        //Right item
        occupied += 1;
    }
    if occupied_vision(input, i, Direction::RightUp, limit) {
        //Top Right Item
        occupied += 1;
    }
    if occupied_vision(input, i, Direction::RightDown, limit) {
        occupied += 1;
    }
    if occupied_vision(input, i, Direction::Up, limit) {
        occupied += 1;
    }
    if occupied_vision(input, i, Direction::Down, limit) {
        occupied += 1;
    }

    occupied
}

fn part1(mut input: Vec<u8>) {
    loop {
        let other: Vec<_> = input
            .iter()
            .enumerate()
            .map(|(i, &v)| {
                if v == b'L' && occupied(&input, i, Some(1)) == 0 {
                    b'#'
                } else if v == b'#' && occupied(&input, i, Some(1)) >= 4 {
                    b'L'
                } else {
                    v
                }
            })
            .collect();

        if other == input {
            //DONE
            println!(
                "Occupied seats: {}",
                bytecount::count(&other, b'#')
            );
            break;
        }
        input = other;
    }
}

fn part2(mut input: Vec<u8>) {
    loop {
        let other: Vec<_> = input
            .iter()
            .enumerate()
            .map(|(i, &v)| {
                if v == b'L' && occupied(&input, i, None) == 0 {
                    b'#'
                } else if v == b'#' && occupied(&input, i, None) >= 5 {
                    b'L'
                } else {
                    v
                }
            })
            .collect();

        if other == input {
            //DONE
            println!(
                "Occupied seats: {}",
                bytecount::count(&other, b'#')
            );
            break;
        }
        input = other;
    }
}

fn main() {
    let file = File::open("input").unwrap();
    let mut reader = BufReader::new(file);
    let mut input = Vec::new();
    reader.read_to_end(&mut input).unwrap();
    input = input.iter().filter(|&&v| v != b'\n').copied().collect();
    part1(input.clone());
    part2(input);
}
