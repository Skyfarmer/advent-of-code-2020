import java.io.BufferedReader
import java.io.IOException
import java.io.UncheckedIOException
import java.util.*
import java.util.stream.Stream
import java.util.stream.StreamSupport
import kotlin.NoSuchElementException

fun BufferedReader.multiLine(): Stream<String> {
    val iter = object : Iterator<String> {
        var nextMultiLine: String? = null

        override fun hasNext(): Boolean {
            if (nextMultiLine != null) {
                return true
            } else {
                val result = mutableListOf<String>()
                var buffer: String?
                do {
                    try {
                        buffer = this@multiLine.readLine()
                        if (buffer != null) {
                            result.add(buffer)
                        }
                    } catch (e: IOException) {
                        throw UncheckedIOException(e)
                    }
                } while (buffer != null && buffer.isNotEmpty())

                nextMultiLine =
                    result.joinToString(" ").trim().let { if (it.isNotEmpty()) it else null }
                return nextMultiLine != null
            }
        }

        override fun next(): String {
            return when {
                nextMultiLine != null -> {
                    val multiLine: String = nextMultiLine!!
                    nextMultiLine = null
                    multiLine
                }
                hasNext() -> {
                    next()
                }
                else -> {
                    throw NoSuchElementException()
                }
            }
        }
    }

    return StreamSupport.stream(
        Spliterators.spliteratorUnknownSize(
            iter,
            Spliterator.ORDERED or Spliterator.NONNULL
        ), false
    )
}

fun validate(passport: String): Boolean {
    return passport.split(" ").map {
        val args = it.split(":")
        Pair(args[0], args[1])
    }.filter { (key, v) ->
        when (key) {
            "byr" -> v.toInt() in 1920..2002
            "iyr" -> v.toInt() in 2010..2020
            "eyr" -> v.toInt() in 2020..2030
            "hgt" -> (v.endsWith("cm") && v.dropLast(2).toInt() in 150..193)
                    || (v.endsWith("in") && v.dropLast(2).toInt() in 59..76)
            "hcl" -> v.matches(Regex("#[0-9a-f]{6}"))
            "ecl" -> v in listOf("amb", "blu", "brn", "gry", "grn", "hzl", "oth")
            "pid" -> v.matches(Regex("[0-9]{9}"))
            else -> false
        }
    }.count() == 7
}

fun readPassports(reader: BufferedReader) =
    reader.multiLine().map { validate(it) }.filter { v -> v }.count()


fun main() {
    val reader = java.io.File("input").bufferedReader()

    println("${readPassports(reader)} passports are valid")

}