import timeit

class Node:
    tree = dict()

    def __init__(self, value, successors):
        self.value = value
        self.next = successors
        self.routes = 0
        Node.tree[value] = self

    def find(self, target):
        if self.value == target:
            return self
        elif len(self.next) != 0:
            for v in self.next:
                tmp = v.find(target)
                if tmp is not None:
                    return tmp
        else:
            return None

    def closest(self, c_single=0, c_triple=0):
        if len(self.next) == 0:
            return c_single, c_triple + 1
        elif self.next[0].value - self.value == 1:
            return self.next[0].closest(c_single + 1, c_triple)
        elif self.next[0].value - self.value == 3:
            return self.next[0].closest(c_single, c_triple + 1)
        else:
            return None

    def touch(self, target):
        if self.routes != 0:
            return self.routes
        elif self.value == target:
            return 1
        elif len(self.next) != 0:
            for v in self.next:
                self.routes += v.touch(target)
            return self.routes
        else:
            return 0


# Input must be sorted in ascending order
def create_tree(data):
    if data[0] in Node.tree:
        return Node.tree[data[0]]
    else:
        prev = list()
        if len(data) >= 2 and 1 <= data[1] - data[0] <= 3:
            prev.append(create_tree(data[1:]))
        if len(data) >= 3 and 1 <= data[2] - data[0] <= 3:
            prev.append(create_tree(data[2:]))
        if len(data) >= 4 and 1 <= data[3] - data[0] <= 3:
            prev.append(create_tree(data[3:]))

        return Node(data[0], prev)


def process(file_name):
    with open(file_name) as f:
        adaptors = list(map(int, f.readlines()))
        adaptors.insert(0, 0)
        adaptors.sort()

        nodes = create_tree(adaptors)
        c_single, c_triple = nodes.closest()
        print(c_single * c_triple)
        print("Execution time:", timeit.timeit("nodes.touch(adaptors[-1])", globals=locals()))
        print(nodes.touch(adaptors[-1]))


# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    process("input")
