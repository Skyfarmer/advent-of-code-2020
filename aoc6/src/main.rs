use std::fs::File;
use std::io::{BufRead, BufReader, Read};

struct MultiLine<T: BufRead> {
    reader: T,
}

impl<T: BufRead> Iterator for MultiLine<T> {
    type Item = String;

    fn next(&mut self) -> Option<Self::Item> {
        let mut buffer = String::new();
        loop {
            match self.reader.read_line(&mut buffer) {
                Ok(0) => {
                    if buffer.is_empty() {
                        break None;
                    } else {
                        break Some(buffer.strip_suffix("\n").unwrap().to_owned());
                    }
                }
                Ok(v) if v != 1 => continue,
                Ok(_) => break Some(buffer.strip_suffix("\n\n").unwrap().to_owned()),
                Err(_) => break None,
            }
        }
    }
}

trait TraitMultiLine<T: BufRead> {
    fn multilines(self) -> MultiLine<T>;
}

impl<T: Read> TraitMultiLine<BufReader<T>> for BufReader<T> {
    fn multilines(self) -> MultiLine<BufReader<T>> {
        MultiLine { reader: self }
    }
}

fn analyze_all(data: &str) -> u32 {
    data.split('\n')
        .map(|person| person.bytes().fold(0u32, |acc, b| acc | 1 << (b - b'a')))
        .fold(!0, |acc, v| acc & v)
        .count_ones()
}

fn analyze_any(data: &str) -> u32 {
    data.bytes()
        .filter(|b| b.is_ascii_lowercase())
        .fold(0u32, |acc, b| acc | 1 << (b - b'a'))
        .count_ones()
}

fn main() {
    let f = File::open("input").unwrap();
    let reader = BufReader::new(f);

    let (v1, v2) = reader
        .multilines()
        .fold((0, 0), |(all_sum, any_sum), line| {
            (all_sum + analyze_all(&line), any_sum + analyze_any(&line))
        });

    println!("Total sum of questions: {}", v1);
    println!("Total sum of questions: {}", v2);
}
