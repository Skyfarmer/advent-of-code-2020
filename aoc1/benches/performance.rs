use aoc1::read_input;
use criterion::{criterion_group, criterion_main, Criterion};
use std::collections::{BTreeSet, HashSet};

pub fn criterion_benchmark(c: &mut Criterion) {
    let input: Vec<i32> = read_input().unwrap();

    c.bench_function("Loop", |b| b.iter(|| aoc1::find_loop(&input)));

    let input: BTreeSet<i32> = read_input().unwrap();
    c.bench_function("BTree", |b| b.iter(|| aoc1::find_btree(&input)));

    let input: HashSet<i32> = read_input().unwrap();
    c.bench_function("HashSet", |b| b.iter(|| aoc1::find_hset(&input)));
}

criterion_group!(benches, criterion_benchmark);
criterion_main!(benches);
