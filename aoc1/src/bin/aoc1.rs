fn main() {
    let input = aoc1::read_input().unwrap();

    let (first, second, third) = aoc1::find_btree(&input).unwrap();

    println!(
        "{} + {} + {} = {}",
        first,
        second,
        third,
        first + second + third
    );
    println!("Result: {}", first * second * third);
}
