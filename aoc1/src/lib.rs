use std::collections::{BTreeSet, HashSet};
use std::fs::File;
use std::io::{BufRead, BufReader};
use std::iter::FromIterator;
use std::str::FromStr;

pub fn read_input<T: FromStr + Ord, U: FromIterator<T>>() -> std::io::Result<U>
where
    T::Err: std::fmt::Debug,
{
    let f = File::open("input")?;
    let reader = BufReader::new(f);

    reader
        .lines()
        .map(|v| Ok(v?.parse::<T>().unwrap()))
        .collect()
}

pub fn find_loop(input: &[i32]) -> Option<(i32, i32, i32)> {
    for i in input {
        for j in input {
            for k in input {
                if *i + *j + *k == 2020 {
                    return Some((*i, *j, *k));
                }
            }
        }
    }

    None
}

pub fn find_hset(input: &HashSet<i32>) -> Option<(i32, i32, i32)> {
    let other = input.iter().find_map(|&v1| {
        match input.iter().find(|&v2| input.contains(&(2020 - v2 - v1))) {
            Some(x) => Some((v1, x)),
            None => None,
        }
    })?;

    let other1 = 2020 - other.0 - other.1;
    let other2 = 2020 - other.0 - other1;
    let other3 = 2020 - other.1 - other1;
    Some((other1, other2, other3))
}

pub fn find_btree(input: &BTreeSet<i32>) -> Option<(i32, i32, i32)> {
    let other = input.iter().find_map(|&v1| {
        match input.iter().find(|&v2| input.contains(&(2020 - v2 - v1))) {
            Some(x) => Some((v1, x)),
            None => None,
        }
    })?;

    let other1 = 2020 - other.0 - other.1;
    let other2 = 2020 - other.0 - other1;
    let other3 = 2020 - other.1 - other1;
    Some((other1, other2, other3))
}
