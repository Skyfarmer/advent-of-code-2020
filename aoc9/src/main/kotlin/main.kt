import kotlin.streams.toList

fun find(input: List<Long>, target: Long): Boolean {
    for (i in input) {
        for (j in input) {
            if (i + j == target) {
                return true
            }
        }
    }
    return false
}

fun findCont(input: List<Long>, target: Long): List<Long>? {
    for ((index, value) in input.withIndex()) {
        if(value > target) {
            continue
        }
        var sum = value
        var stop = index

        input.drop(index + 1).takeWhile {
            stop += 1
            sum += it
            sum < target
        }
        if (sum == target) {
            return input.slice(index until stop)
        }
    }

    return null
}

const val preemble_size = 25

fun main() {
    val reader = java.io.File("input").bufferedReader()
    val numbers = reader.lines().map { it.toLong() }.toList()
    for(win in numbers.windowed(preemble_size + 1)) {
        if(!find(win.take(preemble_size), win.last())){
            println("Found invalid number ${win.last()}")
            val result = findCont(numbers, win.last())
            println("Found valid range ${result?.minOrNull()!! + result.maxOrNull()!!}")
        }
    }
}