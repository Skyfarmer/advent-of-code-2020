#include <iostream>
#include <string>
#include <memory>
#include <utility>
#include <vector>
#include <unordered_map>
#include <unordered_set>
#include <fstream>
#include <ranges>
#include <cstring>
#include <charconv>

constexpr auto CONTAIN = " contain";

class Bag {
  using ParentContainer = std::unordered_set<std::shared_ptr<Bag>>;
  using ChildContainer = std::unordered_map<std::shared_ptr<Bag>, size_t>;

  ParentContainer _parents;
  ChildContainer _children;

  auto _add_relations(const ParentContainer &container, ParentContainer &results) -> void {
    for (const auto &c : container) {
      results.insert(c);
      _add_relations(c->_parents, results);
    }
  }

 public:

  [[nodiscard]] auto parents() const -> ParentContainer const & {
    return _parents;
  }

  auto add_parent_relations() {
    ParentContainer results;
    for (const auto &c : _parents) {
      _add_relations(c->_parents, results);
    }
    _parents.merge(results);
  }

  auto count_bags() const -> size_t {
    size_t amount{0};
    for (const auto &c : _children) {
      amount += (c.first->count_bags() + 1) * c.second;
    }

    return amount;
  }

  static auto add_child_parent(std::shared_ptr<Bag> &parent, std::shared_ptr<Bag> &child, const size_t amount) {
    parent->_children[child] = amount;
    child->_parents.insert(parent);
  }
};

class Rules {
  using Storage = std::unordered_map<std::string, std::shared_ptr<Bag>>;
  Storage _bags;
 public:

  auto add_bag(const std::string &name) -> std::shared_ptr<Bag> {
    return _bags.emplace(std::make_pair(name, std::make_shared<Bag>())).first->second;
  }

  auto bags() const -> Storage const & {
    return _bags;
  }
};

class Line : public std::string {
  friend auto operator>>(std::istream &is, Line &line) -> std::istream & {
    return std::getline(is, line);
  }
};

auto strvtoi(const std::string_view &v) -> std::optional<int> {
  int tmp;
  auto result = std::from_chars(v.data(), v.data() + v.size(), tmp);
  return result.ec == std::errc::invalid_argument ? std::nullopt : std::optional(tmp);
}

auto extract_arguments(const std::string_view &input,
                       const char c,
                       bool start_from_front = true) -> std::optional<std::tuple<std::string_view, std::string_view>> {
  auto pos = start_from_front ? input.find(c) : input.rfind(c);
  if (pos == std::string::npos) {
    return {};
  } else {
    std::string_view front(input.data(), pos);
    std::string_view back(input.data() + pos + 1, input.size() - pos - 1);
    return std::make_tuple(front, back);
  }
}

auto extract_name(const std::string_view &input) -> std::optional<std::string_view> {
  //So here it gets complicated
  //At this point, input must be of this form: "3 shiny gold bag"
  //We need to remove the number first.
  //After this we have: "shiny gold bag"
  //Now we need to get rid of the " bag"
  //Since the value can also be " bags", we need to look for the delimiter from the right.
  auto tmp = extract_arguments(input, ' ', false);
  if (!tmp.has_value()) {
    return {};
  }
  return std::get<0>(tmp.value());
}

auto
process_content_bags(const std::string_view &input, Rules *rules,
                     std::shared_ptr<Bag> &parent) -> void {
  if (input.empty()) {
    return;
  }
  auto until = input.find(',');
  if (until == std::string::npos) {
    until = input.find('.');
    if (until == std::string::npos) {
      throw std::runtime_error("Missing delimiter");
    }
  }
  //We set a + 1 offset, as the first character always is a space
  auto[number, bag] = extract_arguments(std::string_view(input.data() + 1, until - 1), ' ').value();
  bag = extract_name(bag).value();
  auto val_number = strvtoi(number).value_or(0);

  std::shared_ptr<Bag> b;
  if (!rules->bags().contains(std::string(bag))) {
    //Create bag as it does not yet exist
    b = rules->add_bag(std::string(bag));
  } else {
    //Retrieve bag
    b = rules->bags().at(std::string(bag));
  }
  Bag::add_child_parent(parent, b, val_number);
  process_content_bags(std::string_view(input.data() + until + 1), rules, parent);

}

auto process(const std::string &input, Rules *rules) {
  auto pos_target_bag = input.find(CONTAIN);
  if (pos_target_bag == std::string::npos) {
    throw std::runtime_error("Missing 'contain'");
  } else {
    std::string_view target_bag(input.data(), pos_target_bag);
    target_bag = extract_name(target_bag).value();

    std::shared_ptr<Bag> container;
    if (!rules->bags().contains(std::string(target_bag))) {
      container = rules->add_bag(std::string(target_bag));
    } else {
      container = rules->bags().at(std::string(target_bag));
    }
    std::string_view content_bags(input.data() + pos_target_bag + std::strlen((CONTAIN)));
    process_content_bags(content_bags, rules, container);

  }
}

auto main() -> int {
  std::ifstream s("input");
  if (s.is_open()) {
    auto it = std::istream_iterator<Line>(s);
    Rules r{};
    std::for_each(it, std::istream_iterator<Line>(), [&](auto &l) {
      try {
        process(l, &r);
      } catch (const std::runtime_error &e) {
        std::cerr << "ERROR: " << e.what() << std::endl;
      }
      std::cout << l << std::endl;
    });

    auto bag = r.bags().at("shiny gold");
    bag->add_parent_relations();
    std::cout << "Bag can be contained in " << bag->parents().size() << " different bags\n";
    std::cout << "Bag must contain " << bag->count_bags() << " bags\n";
  }

  return 0;
}
