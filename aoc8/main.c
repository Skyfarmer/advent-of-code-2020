#include <stdio.h>
#include "program.h"
#include "state.h"


int main() {
  FILE *f = fopen("input", "r");
  struct Program *p = readProgram(f);

  for(size_t i = 0; i < p->amount; i++) {
    switch(p->instructions[i].opcode) {
      case acc:
        continue;
      case jmp:
        p->instructions[i].opcode = nop;
        break;
      case nop:
        p->instructions[i].opcode = jmp;
        break;
    }
    struct MachineState state = {};
    u_int64_t run[10] = {};
    while((run[state.pc / 64] >> state.pc % 64 & 1) == 0 && state.pc != p->amount) {
      run[state.pc / 64] |= 1ul << state.pc % 64;
      execute(&state, p);
    }

    if(state.pc == p->amount) {
      printf("Accumulator: %i, changed %zu\n", state.accumulator, i);
      break;
    }

    switch (p->instructions[i].opcode) {
      case acc: break;
      case jmp:
        p->instructions[i].opcode = nop;
        break;
      case nop:
        p->instructions[i].opcode = jmp;
        break;
    }
  }


  freeProgram(p);
  return 0;
}
