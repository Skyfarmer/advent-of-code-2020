//
// Created by shimmelbauer on 08.12.20.
//

#ifndef AOC8__STATE_H_
#define AOC8__STATE_H_

#include <stdlib.h>
struct Program;
struct MachineState {
  int32_t accumulator;
  size_t pc;
};

extern void execute(struct MachineState *state, const struct Program *program);

#endif //AOC8__STATE_H_
