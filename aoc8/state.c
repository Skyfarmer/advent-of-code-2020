//
// Created by shimmelbauer on 08.12.20.
//
#include "state.h"
#include "program.h"

void execute(struct MachineState *state, const struct Program *program) {
  struct Instruction *instruction = &program->instructions[state->pc];
  switch (instruction->opcode) {
    case acc:state->accumulator += instruction->immediate;
      state->pc += 1;
      break;
    case jmp:state->pc += instruction->immediate;
      break;
    case nop:state->pc += 1;
      break;
  }
}