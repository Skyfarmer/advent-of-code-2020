//
// Created by shimmelbauer on 08.12.20.
//

#ifndef AOC8__PROGRAM_H_
#define AOC8__PROGRAM_H_

#include <stdlib.h>
#include <stdio.h>

enum Opcode {
  nop,
  acc,
  jmp,
};

struct Instruction {
  int32_t immediate;
  enum Opcode opcode;
};

struct Program {
  struct Instruction *instructions;
  size_t amount;
  FILE *source;
};

extern struct Program *readProgram(FILE *f);
extern void freeProgram(struct Program *program);


#endif //AOC8__PROGRAM_H_
