//
// Created by shimmelbauer on 08.12.20.
//
#include <string.h>
#include <errno.h>
#include "program.h"

#define BUFFER_SIZE 32
#define CHECK_NULL(expr) if(expr == NULL) return NULL

enum Opcode string_to_opcode(char *buffer) {
  if (strncmp(buffer, "nop", 3) == 0) {
    return nop;
  } else if (strncmp(buffer, "acc", 3) == 0) {
    return acc;
  } else if (strncmp(buffer, "jmp", 3) == 0) {
    return jmp;
  } else {
    abort();
  }
}

struct Program *reallocProgram(struct Program *program, size_t amount) {
  struct Instruction *instruction = realloc(program->instructions, sizeof(struct Instruction) * amount);
  if (instruction == NULL) {
    freeProgram(program);
    return NULL;
  } else {
    program->instructions = instruction;
    program->amount = amount;
    return program;
  }
}

struct Program *readProgram(FILE *f) {
  struct Program *program = (struct Program *) malloc(sizeof(struct Program));
  CHECK_NULL(program);
  program->instructions = (struct Instruction *) malloc(sizeof(struct Instruction) * 4096);
  CHECK_NULL(program->instructions);
  program->amount = 4096;
  program->source = f;

  char buffer[BUFFER_SIZE];
  for (size_t i = 0; i < program->amount; i++) {
    if (fgets(buffer, sizeof buffer, f) == NULL) {
      if (feof(f)) {
        //We finished earlier,resize
        return reallocProgram(program, i);
      } else {
        freeProgram(program);
        return NULL;
      }
    } else {
      char *delimiter = strchr(buffer, ' ');
      if (delimiter == NULL) {
        fprintf(stderr, "Invalid instruction at '%s'\n", buffer);
        exit(-1);
      }
      program->instructions[i].opcode = string_to_opcode(buffer);
      program->instructions[i].immediate = strtol(delimiter + 1, NULL, 10);
      if (errno) {
        perror(NULL);
        exit(-1);
      }
    }
  }

  return program;
}

void freeProgram(struct Program *program) {
  free(program->instructions);
  fclose(program->source);
  free(program);
}