#include <stdio.h>

#define ROW_SIZE 31
//Includes newline
#define LINE_SIZE (ROW_SIZE + 1)
//Inludes NULL-terminator
#define BUFFER_SIZE (LINE_SIZE + 1)

void skip_lines(int amount, FILE *file) {
  if (amount > 0) {
    if (fseek(file, LINE_SIZE * amount, SEEK_CUR) != 0) {
      perror("Could not seek");
    }
  }
}

int find_trees(int down, int right, FILE *file) {

  //A line is exactly 32 characters long (including newline)
  char buffer[BUFFER_SIZE];

  //Ignore first lines
  skip_lines(down, file);

  int counter = 0;
  int pos = 0;
  while (fgets(buffer, sizeof(buffer), file) != NULL) {
    // - 2 is needed for ignoring the null-terminator and newline character
    pos = (pos + right) % ((int) sizeof(buffer) - 2);
    if (buffer[pos] == '#') {
      counter += 1;
    }

    skip_lines(down - 1, file);
  }

  printf("Encountered %d trees\n", counter);

  return counter;
}

int main() {
  FILE *f = fopen("input", "rb");
  int parameters[][2] = {{1, 1}, {1, 3}, {1, 5}, {1, 7}, {2, 1}};

  long sum = 1;
  for (int i = 0; i < 5; i++) {
    sum *= find_trees(parameters[i][0], parameters[i][1], f);
    rewind(f);
  }

  printf("Encountered %ld trees\n", sum);

  fclose(f);
  return 0;
}
