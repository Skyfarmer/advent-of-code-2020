#include <iostream>
#include <string>
#include <fstream>
#include <iterator>
#include <charconv>
#include <cmath>

enum class FaceDirection {
    East,
    South,
    West,
    North
};

class Position {
    int x{};
    int y{};
    FaceDirection direction{FaceDirection::East};
public:
    auto move(int amount) {
        move(amount, this->direction);
    }

    auto move(int amount, FaceDirection dir) -> void {
        switch (dir) {
            case FaceDirection::East:
                x += amount;
                break;
            case FaceDirection::West:
                x -= amount;
                break;
            case FaceDirection::North:
                y += amount;
                break;
            case FaceDirection::South:
                y -= amount;
                break;
        }
    }

    auto turn(int amount) {
        direction = static_cast<FaceDirection>((static_cast<int>(direction) + amount / 90) % 4u);
    }

    [[nodiscard]] auto distance() const -> int {
        return std::abs(x) + std::abs(y);
    }
};

class RelativePosition {
    int x_offset{10};
    int y_offset{1};

    Position shipPosition;
public:
    [[nodiscard]] auto getShip() const -> Position const & {
        return shipPosition;
    }

    auto move(int amount, FaceDirection direction) -> void {
        switch (direction) {
            case FaceDirection::East:
                x_offset += amount;
                break;
            case FaceDirection::West:
                x_offset -= amount;
                break;
            case FaceDirection::North:
                y_offset += amount;
                break;
            case FaceDirection::South:
                y_offset -= amount;
                break;
        }
    }

    auto turn_clockwise(int amount) {
        switch (amount) {
            case 90:
                std::swap(x_offset, y_offset);
                y_offset = -y_offset;
                break;
            case 180:
                x_offset = -x_offset;
                y_offset = -y_offset;
                break;
            case 270:
                std::swap(x_offset, y_offset);
                x_offset = -x_offset;
                break;
            default:
                std::cerr << "UNREACHABLE!\n";
        }
    }

    auto move(int amount) {
        shipPosition.move(amount * x_offset, FaceDirection::East);
        shipPosition.move(amount * y_offset, FaceDirection::North);
    }

};

class Line : public std::string {
    friend auto operator>>(std::istream &is, Line &line) -> std::istream & {
        return std::getline(is, line);
    }
};

auto to_string(const std::string &str) -> int {
    int result;
    if (auto[p, ec] = std::from_chars(str.data() + 1, str.data() + str.size(), result); ec == std::errc()) {
        return result;
    } else {
        return 0;
    }
}

auto main() -> int {
    std::ifstream s("input");
    if (s.is_open()) {
        auto it = std::istream_iterator<Line>(s);
        Position pos;
        RelativePosition posRel;

        std::for_each(it, std::istream_iterator<Line>(), [&](auto &line) {
            int num = to_string(line);
            switch (line[0]) {
                case 'N':
                    pos.move(num, FaceDirection::North);
                    posRel.move(num, FaceDirection::North);
                    break;
                case 'S':
                    pos.move(num, FaceDirection::South);
                    posRel.move(num, FaceDirection::South);
                    break;
                case 'E':
                    pos.move(num, FaceDirection::East);
                    posRel.move(num, FaceDirection::East);
                    break;
                case 'W':
                    pos.move(num, FaceDirection::West);
                    posRel.move(num, FaceDirection::West);
                    break;
                case 'L':
                    pos.turn(-num);
                    posRel.turn_clockwise(360 - num);
                    break;
                case 'R':
                    pos.turn(num);
                    posRel.turn_clockwise(num);
                    break;
                case 'F':
                    pos.move(num);
                    posRel.move(num);
                    break;
                default:
                    std::cerr << "Unknown direction command " << line[0] << "\n";
            }
        });
        std::cout << "Distance: " << pos.distance() << "\n";
        std::cout << "Distance: " << posRel.getShip().distance() << "\n";
    }
    return 0;
}
