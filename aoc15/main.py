def determine(numbers, upper):
    turn_map = dict()
    for idx, n in enumerate(numbers):
        turn_map[n] = idx + 1

    last_num = 0
    for idx in range(len(numbers) + 2, upper):
        if last_num in turn_map:
            tmp = idx - 1 - turn_map[last_num]
            turn_map[last_num] = idx - 1
            last_num = tmp
        else:
            turn_map[last_num] = idx - 1
            last_num = 0

    return last_num


# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    with open("input", "r") as f:
        values = list(map(int, f.readline().split(",")))
        l_name = determine(values, 2021)
        print("2020th number:", l_name)

        l_name = determine(values, 30000001)
        print("30000000th number:", l_name)
