use std::fs::File;
use std::io::{BufRead, BufReader};
use std::ops::{Deref, RangeInclusive};

#[derive(Clone)]
struct Rule<T: Clone> {
    name: String,
    range: Vec<RangeInclusive<T>>,
    indices: u32,
}

impl<T:Clone> Rule<T> {
    fn get_next(&self) -> usize {
        for i in 0..32 {
            if (self.indices >> i) & 0x1 == 1 {
                return i
            }
        }
        0
    }
}

impl<T: PartialOrd + Clone> Rule<T> {
    fn contains(&self, value: T) -> bool {
        self.range.iter().any(|v| v.contains(&value))
    }
}

struct Ticket {
    fields: Vec<i32>,
}

impl From<&str> for Ticket {
    fn from(line: &str) -> Self {
        Ticket {
            fields: line.split(',').map(|v| v.parse().unwrap()).collect(),
        }
    }
}

fn split<'a>(input: &'a str, c: &str) -> (&'a str, &'a str) {
    let pos = input.find(c).expect("Expected :");
    (&input[..pos], &input[pos + c.len()..])
}

fn generate_range(input: &str) -> RangeInclusive<i32> {
    let (v1, v2) = split(input, "-");
    let (v1, v2) = (v1.trim().parse().unwrap(), v2.trim().parse().unwrap());
    v1..=v2
}

fn main() {
    let f = File::open("input").unwrap();
    let reader = BufReader::new(f);
    let mut rules: Vec<Rule<_>> = Vec::new();
    let mut tickets = Vec::new();
    let mut my_ticket = None;

    let mut analyze_tickets = false;
    let mut your_ticket = false;
    let mut missing = 0;
    for v in reader.lines() {
        let line = v.unwrap();
        if line.is_empty() {
            continue;
        } else if line == "your ticket:" {
            your_ticket = true;
            continue;
        } else if line == "nearby tickets:" {
            analyze_tickets = true;
            continue;
        } else if analyze_tickets {
            let tmp_ticket = Ticket::from(line.deref());
            let tmp = tmp_ticket
                .fields
                .iter()
                .filter(|v| !rules.iter().any(|rule| rule.contains(**v)))
                .sum::<i32>();
            missing += tmp;
            if tmp == 0 {
                tickets.push(tmp_ticket);
            }
        } else if your_ticket {
            your_ticket = false;
            my_ticket = Some(Ticket::from(line.deref()));
        } else {
            let (name, rest) = split(&line, ":");
            let (range1, range2) = split(rest, "or");
            let (range1, range2) = (generate_range(range1), generate_range(range2));
            rules.push(Rule {
                name: name.to_owned(),
                range: [range1, range2].into(),
                indices: 0,
            });
        }
    }

    for idx in 0..tickets.first().unwrap().fields.len() {
        for rule in &mut rules {
            if tickets.iter().all(|v| rule.contains(v.fields[idx])) {
                rule.indices |= 1 << idx;
            }
        }
    }

    for _ in 0..rules.len() {
        for i in 0..rules.len() {
            match rules
                .iter()
                .find(|v| v.indices.count_ones() == 1 && (v.indices & 1 << i) != 0)
            {
                Some(v) => {
                    let v = v.clone();
                    rules.iter_mut().for_each(|r| {
                        if r.name != v.name {
                            r.indices &= !(1 << i);
                        }
                    });
                }
                None => continue,
            }
        }
    }

    println!("Result {}", missing);
    let my_ticket = my_ticket.unwrap();
    for rule in &mut rules {
        if rule.indices.count_ones() == 0 {
            rule.indices = 1 << 7;
        }
    }

/*    let dtrack = rules.iter().find(|v| v.name == "departure track").unwrap();
    if tickets.iter().all(|v| {
        let r = dtrack.contains(v.fields[7]);
        if !r {
            println!("{} is not contained", v.fields[7]);
        }
        r
    }) {
        println!("HELLO");
    }*/
    let result: i64 = rules
        .iter()
        .filter(|rule| rule.name.starts_with("departure"))
        .map(|rule| my_ticket.fields[rule.get_next()] as i64)
        .product();
    println!("Product {}", result);
}
