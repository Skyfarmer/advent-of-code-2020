def process(data, lower, upper, low_identifier, high_identifier):
    row_range = range(lower, upper)
    for c in data:
        if c == low_identifier:
            row_range = range(row_range.start, row_range.start + len(row_range) // 2)
        elif c == high_identifier:
            row_range = range(row_range.start + (len(row_range) + 1) // 2, row_range.stop)
        else:
            raise ValueError("Invalid row identifier: %r" % c)

    if data[-1] == low_identifier:
        return row_range.start
    else:
        return row_range.stop


def process_row(data):
    return process(data, 0, 127, 'F', 'B')


def process_column(data):
    return process(data, 0, 7, 'L', 'R')


# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    with open("input") as f:
        max = 0
        passes = []
        for line in f:
            row = process_row(line[:7])
            column = process_column(line[7:-1])
            tmp = row * 8 + column
            if max < tmp:
                max = tmp
            passes.append(tmp)

        print("Max ID:", max)
        passes.sort()
        last = passes[0]
        for bpass in passes[1:]:
            if bpass - 1 != last:
                print("Found missing board pass:", bpass - 1)
                break
            else:
                last = bpass

# See PyCharm help at https://www.jetbrains.com/help/pycharm/
