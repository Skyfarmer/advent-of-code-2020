#include <iostream>
#include <fstream>
#include <vector>
#include <iterator>

class Line : public std::string {
  friend auto operator>>(std::istream &is, Line &line) -> std::istream & {
    return std::getline(is, line);
  }
};

class Cube {
  bool active;
 public:
  [[maybe_unused]] explicit Cube(bool active) : active(active) {}

  [[nodiscard]] auto is_active() const {
    return active;
  }

  auto set_active(bool _active) {
    this->active = _active;
  }
};

using CubeLine = std::vector<Cube>;
using CubeGrid = std::vector<CubeLine>;
using CubeSpace = std::vector<CubeGrid>;
using CubeHyper = std::vector<CubeSpace>;

auto count_line_neighbours(const CubeLine &line, int at, bool itself) {
  int counter = 0;
  //Check itself
  if (itself && line.at(at).is_active()) {
    counter += 1;
  }
  //Check in line left
  if (at != 0 && line.at(at - 1).is_active()) {
    counter += 1;
  }
  //Check in line right
  if (at != line.size() - 1 && line.at(at + 1).is_active()) {
    counter += 1;
  }
  return counter;
}

auto count_grid_neighbours(const CubeGrid &grid, int x, int y, bool itself) {
  auto counter = 0;
  //Check in line
  counter += count_line_neighbours(grid.at(y), x, itself);
  if (y != 0) {
    counter += count_line_neighbours(grid.at(y - 1), x, true);
  }
  if (y != grid.size() - 1) {
    counter += count_line_neighbours(grid.at(y + 1), x, true);
  }

  return counter;
}

auto count_space_neighbours(const CubeSpace &space, int x, int y, int z, bool itself) {
  auto counter = 0;
  counter += count_grid_neighbours(space.at(z), x, y, itself);
  if (z != 0) {
    counter += count_grid_neighbours(space.at(z - 1), x, y, true);
  }
  if (z != space.size() - 1) {
    counter += count_grid_neighbours(space.at(z + 1), x, y, true);
  }

  return counter;
}

auto count_hyper_neighbours(const CubeHyper &hyper, int x, int y, int z, int w) {
  auto counter = 0;
  counter += count_space_neighbours(hyper.at(w), x, y, z, false);
  if (w != 0) {
    counter += count_space_neighbours(hyper.at(w - 1), x, y, z, true);
  }
  if (w != hyper.size() - 1) {
    counter += count_space_neighbours(hyper.at(w + 1), x, y, z, true);
  }

  return counter;
}

auto cycle(const CubeSpace &space) {
  auto new_space = space;
  for (int i = 0; i < space.size(); i++) {
    auto &grid = space.at(i);
    for (int j = 0; j < grid.size(); j++) {
      auto &line = grid.at(j);
      for (int k = 0; k < line.size(); k++) {
        auto counter = count_space_neighbours(space, k, j, i, false);
        auto &cube = new_space.at(i).at(j).at(k);
        if (cube.is_active() && counter != 2 && counter != 3) {
          cube.set_active(false);
        } else if (!cube.is_active() && counter == 3) {
          cube.set_active(true);
        }
      }
    }
  }

  return new_space;
}

auto cycle(const CubeHyper &hyper) {
  auto new_hyper = hyper;
  for (int i = 0; i < hyper.size(); i++) {
    auto &space = hyper.at(i);
    for (int j = 0; j < space.size(); j++) {
      auto &grid = space.at(j);
      for (int k = 0; k < grid.size(); k++) {
        auto &line = grid.at(k);
        for (int l = 0; l < line.size(); l++) {
          auto counter = count_hyper_neighbours(hyper, l, k, j, i);
          auto &cube = new_hyper.at(i).at(j).at(k).at(l);
          if (cube.is_active() && counter != 2 && counter != 3) {
            cube.set_active(false);
          } else if (!cube.is_active() && counter == 3) {
            cube.set_active(true);
          }
        }
      }
    }
  }

  return new_hyper;
}

auto add_halo(CubeLine &line) {
  line.emplace(line.begin(), false);
  line.emplace_back(false);
}

auto add_halo(CubeGrid &grid) {
  for (auto &line : grid) {
    add_halo(line);
  }
  grid.emplace(grid.begin(), grid.at(0).size(), Cube(false));
  grid.emplace_back(grid.at(0).size(), Cube(false));
}

auto add_halo(CubeSpace &space) {
  for (auto &grid : space) {
    add_halo(grid);
  }
  space.emplace(space.begin(), space.at(0).size(), CubeLine(space.at(0).at(0).size(), Cube(false)));
  space.emplace_back(space.at(0).size(), CubeLine(space.at(0).at(0).size(), Cube(false)));
}

auto add_halo(CubeHyper &hyper) {
  for (auto &space : hyper) {
    add_halo(space);
  }
  hyper.emplace(hyper.begin(),
                hyper.at(0).size(),
                CubeGrid(hyper.at(0).at(0).size(), CubeLine(hyper.at(0).at(0).at(0).size(), Cube(false))));
  hyper.emplace_back(
      hyper.at(0).size(),
      CubeGrid(hyper.at(0).at(0).size(), CubeLine(hyper.at(0).at(0).at(0).size(), Cube(false))));
}

auto print(const CubeLine &line) {
  for (const auto &cube : line) {
    std::cout << (cube.is_active() ? "#" : ".");
  }
}

auto print(const CubeGrid &grid) {
  for (const auto &line : grid) {
    print(line);
    std::cout << "\n";
  }
}

auto print(const CubeSpace &space) {
  for (int i = 0; i < space.size(); i++) {
    std::cout << "Grid " << i << "\n";
    print(space[i]);
    std::cout << "\n";
  }
}

auto print(const CubeHyper &hyper) {
  for (int i = 0; i < hyper.size(); i++) {
    std::cout << "Hyper " << i << "\n";
    print(hyper[i]);
  }
}

auto count(const CubeSpace &space) {
  auto counter = 0;
  for (const auto &grid : space) {
    for (const auto &line : grid) {
      for (const auto &cube : line) {
        if (cube.is_active()) {
          counter += 1;
        }
      }
    }
  }

  return counter;
}

auto count(const CubeHyper &hyper) {
  auto counter = 0;
  for (const auto &space : hyper) {
    counter += count(space);
  }

  return counter;
}

auto main() -> int {
  std::ifstream input("input");
  if (input.is_open()) {
    CubeGrid grid;
    auto it = std::istream_iterator<Line>(input);
    std::for_each(it, std::istream_iterator<Line>(), [&](auto &line) {
      //initialize with one halo element
      CubeLine tmp;
      for (const auto &c : line) {
        switch (c) {
          case '.':tmp.emplace_back(false);
            break;
          case '#':tmp.emplace_back(true);
            break;
          default:std::cerr << "Error at line " << line << "\n";
        }
      }
      grid.push_back(std::move(tmp));
    });
    CubeSpace space;
    space.push_back(std::move(grid));

    CubeHyper hyper;
    hyper.push_back(space);

    add_halo(space);
    add_halo(hyper);

    for (int i = 0; i < 6; i++) {
      std::cout << "Cycle " << i << "\n";
      space = cycle(space);
      hyper = cycle(hyper);
      add_halo(space);
      add_halo(hyper);
    }

    std::cout << "Active cubes: " << count(space) << "\n";
    std::cout << "Active cubes: " << count(hyper) << "\n";
  }
  return 0;
}
