cmake_minimum_required(VERSION 3.17)
project(aoc17)

set(CMAKE_CXX_STANDARD 20)

add_executable(aoc17 main.cpp)