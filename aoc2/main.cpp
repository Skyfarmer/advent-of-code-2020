#include <iostream>
#include <fstream>
#include <iterator>
#include <tuple>
#include <optional>
#include <charconv>
#include <functional>

class Line : public std::string {
    friend auto operator>>(std::istream &is, Line &line) -> std::istream & {
        return std::getline(is, line);
    }
};

auto extract_arguments(const std::string_view &input, const char c) -> std::tuple<std::string_view, std::string_view> {
    auto pos = input.find(c);
    if (pos == std::string::npos) {
        throw std::runtime_error("Invalid encoding");
    } else {
        std::string_view args(input.data(), pos);
        std::string_view password(input.data() + pos + 1, input.size() - pos - 1);
        return std::make_tuple(args, password);
    }
}

auto extract_arguments(const std::string &input, const char c) -> std::tuple<std::string_view, std::string_view> {
    return extract_arguments(std::string_view(input), c);
}

auto strvtoi(const std::string_view &v) -> std::optional<int> {
    int tmp;
    auto result = std::from_chars(v.data(), v.data() + v.size(), tmp);
    return result.ec == std::errc::invalid_argument ? std::nullopt : std::optional(tmp);
}

template<typename T>
class Validator {
    const int first;
    const int second;
    const char c;
    const T func;

public:
    Validator(int first, int second, char c, T func) : first{first}, second{second}, c{c}, func{std::move(func)} {}

    [[nodiscard]] auto is_valid(const std::string_view &input) const -> bool {
        return func(input, first, second, c);
    }
};

auto main(int argc, char *argv[]) -> int {
    std::function<bool (const std::string_view &, int, int, char)> valid_func;
    if(argc >= 2) {
        if(argv[1] == std::string("-range")) {
            valid_func = [](const auto &input, auto min, auto max, auto c) {
                int counter{0};
                for(const auto &v : input) {
                    if(v == c) {
                        counter += 1;
                    }
                }

                return counter >= min && counter <= max;
            };
        } else if(argv[1] == std::string("-index")) {
            valid_func = [](const auto &input, auto first, auto second, auto c) {
                return (input.at(first) == c || input.at(second) == c) && input.at(first) != input.at(second);
            };
        } else {
            std::cerr << "Invalid mode requested\n";
            return -1;
        }
    } else {
        std::cerr << "No validation mode requested\n";
        return -1;
    }

    std::ifstream s("input");
    if (s.is_open()) {
        auto it = std::istream_iterator<Line>(s);
        auto valid_passwords{0};
        std::for_each(it, std::istream_iterator<Line>(), [&](auto &l) {
            auto[args, password] = extract_arguments(l, ':');
            auto[numbers, character] = extract_arguments(args, ' ');
            auto[min, max] = extract_arguments(numbers, '-');

            int int_min, int_max;
            try {
              int_min = strvtoi(min).value();
              int_max = strvtoi(max).value();
            } catch(const std::runtime_error &e) {
              std::cerr << "Conversion to integer failed: " << e.what() << "\n";
              return;
            }
            Validator validator(int_min, int_max, character[0], valid_func);
            if(validator.is_valid(password)) {
                valid_passwords += 1;
            }
        });

        std::cout << valid_passwords << " passwords are valid\n";
    }
    return 0;
}
