#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include <assert.h>

enum Operand {
  Add,
  Mul
};

enum Type {
  Bi,
  Const
};

struct BiOperator {
  enum Operand op;
  struct Operation *left;
  struct Operation *right;
};

struct Operation {
  enum Type type;
  union {
    struct BiOperator bi;
    long constant;
  };
};

struct Operation *new_operation() {
  struct Operation *tmp = (struct Operation *) malloc(sizeof(struct Operation));
  memset(tmp, 0, sizeof(struct Operation));
  return tmp;
}

void free_operation(struct Operation *operation) {
  switch (operation->type) {
    case Const:
      free(operation);
      break;
    case Bi:
      if(operation->bi.left != NULL) {
        free_operation(operation->bi.left);
      }
      if(operation->bi.right != NULL) {
        free_operation(operation->bi.right);
      }
      free(operation);
      break;
  }
}

struct Operation *parse(const char *buffer, size_t size, size_t *amount);

struct Operation *parse_with_priority(const char *buffer, size_t size, size_t *amount) {
  struct Operation *operation = NULL;
  for (size_t i = 0; i < size;) {
    struct Operation *tmp = parse(buffer + i, size - i, amount);
    if (operation == NULL) {
      operation = tmp;
    } else if (operation->type == Bi && operation->bi.right == NULL) {
      operation->bi.right = tmp;
    } else if (tmp->type == Bi) {
      tmp->bi.left = operation;
      operation = tmp;
    }

    i += *amount + 1;
    if (buffer[i] == ')' || buffer[i] == '\n') {
      *amount = i;
      break;
    }
  }

  return operation;
}

struct Operation *parse(const char *buffer, size_t size, size_t *amount) {
  for (size_t i = 0; i < size; i++) {
    if (buffer[i] == '(') {
      struct Operation *tmp = parse_with_priority(buffer + i + 1, size - i - 1, amount);
      *amount += i + 1;
      return tmp;
    } else if (buffer[i] == '+') {
      struct Operation *tmp = new_operation();
      tmp->type = Bi;
      tmp->bi.op = Add;

      *amount = i;
      return tmp;
    } else if (buffer[i] == '*') {
      struct Operation *tmp = new_operation();
      tmp->type = Bi;
      tmp->bi.op = Mul;

      *amount = i;
      return tmp;
    } else if (isdigit(buffer[i])) {
      struct Operation *tmp = new_operation();
      tmp->type = Const;
      tmp->constant = buffer[i] - '0';
      *amount = i;
      return tmp;
    }
  }

  return NULL;
}

long evaluate(const struct Operation *operation) {
  switch (operation->type) {
    case Bi: {
      long left = evaluate(operation->bi.left);
      long right = evaluate(operation->bi.right);
      switch (operation->bi.op) {
        case Add:return left + right;
        case Mul:return left * right;
      }
    }
      break;
    case Const:return operation->constant;
  }
}

void add_parenthesis(char *buffer, size_t at) {
  assert(buffer[at] == '+');
  //Search forward
  int counter = 0;
  for (size_t i = at; i >= 0; i--) {
    if (buffer[i] == ')') {
      counter += 1;
    } else if (buffer[i] == '(') {
      counter -= 1;
      if (counter == 0) {
        memmove(buffer + i + 1, buffer + i, strlen(buffer + i) + 1);
        buffer[i] = '(';
        at = at + 1;
        break;
      }
    } else if (isdigit(buffer[i]) && counter == 0) {
      //Left operand is digit
      memmove(buffer + i + 1, buffer + i, strlen(buffer + i) + 1);
      buffer[i] = '(';
      at = at + 1;
      break;
    }
  }

  counter = 0;
  for (size_t i = at; i < strlen(buffer); i++) {
    if (buffer[i] == '(') {
      counter += 1;
    } else if (buffer[i] == ')') {
      counter -= 1;
      if (counter == 0) {
        memmove(buffer + i + 1, buffer + i, strlen(buffer + i) + 1);
        buffer[i + 1] = ')';
        break;
      }
    } else if (isdigit(buffer[i]) && counter == 0) {
      //Left operand is digit
      memmove(buffer + i + 1, buffer + i, strlen(buffer + i) + 1);
      buffer[i + 1] = ')';
      break;
    }
  }
}

int main() {
  FILE *f = fopen("input", "r");
  if (f) {
    long long sum = 0;
    long long sum_precedence = 0;
    while (!feof(f)) {
      char buffer[4096];
      if (fgets(buffer, sizeof(buffer), f)) {
        size_t until = 0;
        struct Operation *operation = parse_with_priority(buffer, sizeof(buffer), &until);
        sum += evaluate(operation);
        free_operation(operation);

        char *tmp = strchr(buffer, '+');
        while (tmp) {
          add_parenthesis(buffer, tmp - buffer);
          tmp = strchr(tmp + 2, '+');
        }

        printf("%s", buffer);
        until = 0;
        operation = parse_with_priority(buffer, sizeof(buffer), &until);
        sum_precedence += evaluate(operation);
        free_operation(operation);
      }
    }
    printf("%lli\n", sum);
    printf("%lli\n", sum_precedence);
  }
  return 0;
}
