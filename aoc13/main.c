#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#include <math.h>

struct Bus {
  long id;
  long earliest;
};

long inv_mod(long x, long p) {
  long result = 1;
  for(long i = 0; i < p - 2; i++) {
    result = (result * x) % p;
  }
  return result;
}

size_t process2(char *buffer, long *bus) {
  if (buffer[0] == '\0') {
    return 0;
  } else if (buffer[0] == 'x') {
    *bus = -1;
    return process2(buffer + 2, bus + 1) + 1;
  } else {
    char *end;
    long id = strtol(buffer, &end, 10);
    if (end == buffer) {
      perror("Could not read input");
    } else {
      *bus = id;
      return process2(end + 1, bus + 1) + 1;
    }
  }
}

void process(char *buffer, struct Bus *bus, const long earliest) {
  if (buffer[0] == '\0') {
    return;
  } else if (buffer[0] == 'x') {
    fprintf(stderr, "Found x, skipping...\n");
    process(buffer + 2, bus, earliest);
    return;
  } else {
    char *end;
    long id = strtol(buffer, &end, 10);
    if (end == buffer) {
      perror("Could not read input");
    } else {
      long multi = lroundl((earliest / (long double) id) + 0.5);
      long difference = (multi * id) - earliest;
      if (bus->earliest > difference) {
        bus->id = id;
        bus->earliest = difference;
      }
      process(end + 1, bus, earliest);
    }
  }
}

int main() {
  FILE *f = fopen("input", "r");

  char buffer[4096];
  if (fgets(buffer, sizeof(buffer), f)) {
    long earliest = strtol(buffer, NULL, 10);
    struct Bus bus = {.id = 0, .earliest = LONG_MAX};

    if (fgets(buffer, sizeof buffer, f)) {
      process(buffer, &bus, earliest);

      long *indexed_buses = (long *) malloc(sizeof(long) * 1000);
      size_t amount = process2(buffer, indexed_buses);

      // I seriously have no idea what I have done here
      long product = 1;
      for(size_t i = 0; i < amount; i++) {
        if(indexed_buses[i] != -1) {
          product *= indexed_buses[i];
        }
      }

      long sum = 0;
      for(size_t i = 0; i < amount; i++) {
        if(indexed_buses[i] == -1) {
          continue;
        }
        long a = i;
        long b = indexed_buses[i];
        sum += -a * (product / b) * inv_mod(product / b, b);
      }

      printf("Found earliest timestamp: %li\n", ((sum % product) + product) % product);
      free(indexed_buses);
    }

    printf("Found bus with id: %li\n", bus.id * bus.earliest);
  }

  return 0;
}
