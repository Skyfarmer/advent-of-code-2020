import java.io.File

class Mask(
    private val or_mask: Long,
    private val and_mask: Long,
    private val floating: List<Int>
) {

    constructor() : this(0, 0, listOf())

    fun mask(x: Long): Long {
        return (x or or_mask) and and_mask
    }

    private fun maskZero(x: Long, at: Int) = (x and (0x1L shl at).inv()) or (or_mask and (0x1L shl at).inv())
    private fun maskOne(x: Long, at: Int) = x or (or_mask or (0x1L shl at))

    private fun maskFloat(x: Long, result: MutableList<Long>, start: Int) {
        if(start == floating.size) {
            result.add(x)
            return
        } else {
            val zero = maskZero(x, floating[start])
            maskFloat(zero, result, start + 1)
            val one = maskOne(x, floating[start])
            maskFloat(one, result, start + 1)
        }
    }

    fun maskFloat(x: Long): List<Long> {
        val result = mutableListOf<Long>()
        maskFloat(x, result, 0)
        return result
    }
}

infix fun Long.mask(m: Mask): Long = m.mask(this)


fun main() {
    val memory = mutableMapOf<Long, Long>()
    val memory2 = mutableMapOf<Long, Long>()
    var m = Mask()
    File("input").bufferedReader().lines().forEach {
        val input = it.split("=")
        if (input[0].startsWith("mask")) {
            input[1].foldIndexed(Pair(Pair(0L, 0L), mutableListOf<Int>()), { idx, acc, v ->
                when (v) {
                    'X' -> {
                        acc.second.add(36 - idx)
                        Pair(
                            Pair(acc.first.first shl 1, (acc.first.second or 0x1) shl 1),
                            acc.second
                        )
                    }
                    '1' -> Pair(
                        Pair(
                            (acc.first.first or 0x1) shl 1,
                            (acc.first.second or 0x1) shl 1
                        ), acc.second
                    )
                    else -> Pair(Pair(acc.first.first shl 1, acc.first.second shl 1), acc.second)
                }
            }).let { result ->
                m = Mask(result.first.first ushr 1, result.first.second ushr 1, result.second)
            }
        } else if (input[0].startsWith("mem")) {
            val address = input[0].filter { c -> c.isDigit() }.toLong()
            val content = input[1].trim().toLong()
            memory[address] = content mask m

            for (addr in m.maskFloat(address)) {
                memory2[addr] = content
            }
        }
    }

    val result = memory.values.sum()
    println("Result: $result")
    println("Result: ${memory2.values.sum()}")
}