import java.io.File
import java.util.stream.Collectors

interface Rule {
    fun matches(input: String): Pair<Boolean, Int>
}

class Letter(private val character: Char) : Rule {
    override fun matches(input: String): Pair<Boolean, Int> = Pair(input.startsWith(character), 1)
}

class ContainerRule(
    private val name: Int,
    var recursionLimit: Int,
    var isRightRecursive: Boolean,
    var isMiddleRecursive: Boolean,
    var rules: Map<Int, Rule>,
    var subrules: List<List<Int>>,
    private var recRules: List<ContainerRule>
) : Rule {

    constructor(name: Int, subrules: List<List<Int>>) : this(
        name,
        1,
        false,
        false,
        mapOf(),
        subrules,
        listOf()
    )

    private fun matches(input: String, subrules: List<Int>): Pair<Boolean, Int> {
        var start = 0
        val result = subrules.all {
            val rule = rules[it]!!
            val result = rule.matches(input.substring(start))
            start += result.second
            result.first
        }

        return Pair(result, start)
    }

    override fun matches(input: String): Pair<Boolean, Int> {
        var start = 0
        var result = false
        if (isRightRecursive) {
            for (i in 0 until recursionLimit) {
                val tmp = matches(input.substring(start), subrules.first())
                if (tmp.first) {
                    result = tmp.first
                    start += tmp.second
                }
            }
        } else if (isMiddleRecursive) {
            var tmp = Pair(false, 0)
            var tmpCounter = 0
            for (i in 0 until recursionLimit) {
                val subrules = listOf(subrules.first().first())
                tmp = matches(input.substring(tmpCounter), subrules)
                if (!tmp.first) {
                    break
                } else {
                    tmpCounter += tmp.second
                }
            }
            if (tmp.first) {
                for (i in 0 until recursionLimit) {
                    val subrules = listOf(subrules.first()[1])
                    tmp = matches(input.substring(tmpCounter), subrules)
                    if (!tmp.first) {
                        break
                    } else {
                        tmpCounter += tmp.second
                    }
                }
            }
            if (tmp.first) {
                result = tmp.first
                start = tmpCounter
            }
        } else {
            for (r in subrules) {
                val tmp = matches(input, r)
                if (tmp.first) {
                    result = tmp.first
                    start = tmp.second
                }
            }
        }
        return Pair(result, start)
    }

    fun resetLimit() {
        recursionLimit = 1
    }

    fun match(input: String): Boolean {
        val tmp = matches(input)
        val result = tmp.first && tmp.second == input.length
        return when {
            result -> {
                true
            }
            recRules.all { it.recursionLimit < input.length } -> {
                recRules[1].recursionLimit += 1
                if(recRules[1].recursionLimit == input.length) {
                    recRules[0].recursionLimit += 1
                    recRules[1].recursionLimit = 1
                }
                match(input)
            }
            else -> {
                false
            }
        }
    }

    fun populateRecursive() {
        recRules =
            rules.filter { (_, v) -> v is ContainerRule && (v.isMiddleRecursive || v.isRightRecursive) }
                .map { it.value as ContainerRule }.toList()
    }

    fun checkRecursive() {
        subrules = subrules.filter { list ->
            list.all {
                if (it != name) {
                    true
                } else {
                    if (list.size == 3) {
                        isMiddleRecursive = true
                    } else {
                        isRightRecursive = true
                    }
                    false
                }
            }
        }
    }

}

fun parse(input: String): List<Int> =
    input.split(' ').filter { it.isNotEmpty() }.map { it.toInt() }.toList()

fun validate(r: List<String>, input: List<String>): Int {
    val rules = r.map { line ->
        val (key, rule) = line.split(":").let { Pair(it[0].toInt(), it[1]) }
        if (rule.contains("\"")) {
            //It is a final rule
            Pair(key, Letter(rule.find { c -> c.isLetter() }!!))
        } else {
            Pair(key, ContainerRule(key, rule.split("|").map { parse(it) }.toList()))
        }
    }.toMap()

    rules.forEach { (_, value) ->
        if (value is ContainerRule) {
            value.rules = rules
            value.checkRecursive()
        }
    }

    val ruleZero = rules[0] as ContainerRule
    ruleZero.populateRecursive()

    return input.fold(0, { acc, v ->
        val res = if (ruleZero.match(v)) {
            acc + 1
        } else {
            acc
        }
        rules.forEach {
            val value = it.value
            if (value is ContainerRule) {
                value.resetLimit()
            }
        }
        res
    })
}

fun readInput(name: String): Pair<List<String>, List<String>> {
    val input = File(name).bufferedReader().lines().filter { it.isNotEmpty() }
        .collect(Collectors.partitioningBy { it.first().isDigit() })

    return Pair(input[true]!!, input[false]!!)
}

fun main() {
    val (textRules, input) = readInput("input")
    println(validate(textRules, input))

    val (textRules2, input2) = readInput("input2")
    println(validate(textRules2, input2))
}